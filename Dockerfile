FROM python:3.7-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update \
    && apk add --virtual python3-dev \
    && apk add build-base postgresql-dev jpeg-dev libpng-dev \
    && apk add libffi-dev \
    && pip install psycopg2

COPY . /app

WORKDIR /app

RUN pip install --trusted-host pypi.python.org -r requirements.txt

EXPOSE 8000

ENTRYPOINT ["/app/entrypoint.sh"]

CMD ["gunicorn", "-b", "0.0.0.0:8000", "-w", "4", "tester.wsgi:application"]
