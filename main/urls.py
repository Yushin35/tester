from django.conf.urls import url

from .views import (
    TestListView,
    TestDescriptionView,
    TestRunView,
    TestScoreView,
)


urlpatterns = [
    url(r'^$', TestListView.as_view(), name='tests'),
    url(r'^test/(?P<pk>[0-9]+)/$', TestDescriptionView.as_view(), name='test_description'),
    url(r'^test_run/(?P<pk>[0-9]+)/$', TestRunView.as_view(), name='test_run'),
    url(r'^test_score/(?P<pk>[0-9]+)/$', TestScoreView.as_view(), name='test_score'),
]
