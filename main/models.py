from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.

class User(AbstractUser):
    pass


class Test(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(null=True)

    def __str__(self):
        return self.name


class Question(models.Model):
    test = models.ForeignKey(Test, related_name='questions', on_delete=models.CASCADE)
    text = models.TextField()
    type = models.CharField(choices=[
            ('text', 'text'),
            ('select', 'select'),
        ],
        max_length=255,
        default='text',
    )

    def __str__(self):
        return self.text


class Answer(models.Model):
    test = models.ForeignKey(Question, related_name='answers', on_delete=models.CASCADE)
    right = models.BooleanField(default=False)
    value = models.CharField(max_length=255)

    def __str__(self):
        return self.value


class TestRun(models.Model):
    user = models.ForeignKey(User, related_name='test_runs', on_delete=models.CASCADE)
    test = models.ForeignKey(Test, related_name='test_runs', on_delete=models.CASCADE)
    score = models.IntegerField(null=True)
    start_date = models.DateTimeField(auto_now_add=True)
    finish_date = models.DateTimeField(null=True)


class TestRunAnswer(models.Model):
    test_run = models.ForeignKey(TestRun, related_name='answers', on_delete=models.CASCADE)
    question = models.ForeignKey(Question, related_name='+', on_delete=models.CASCADE)
    text = models.CharField(max_length=255, blank=True)
    choice = models.ForeignKey(Answer, on_delete=models.CASCADE, null=True)

    class Meta:
        unique_together = ('test_run', 'question')
