from django import forms


class TestForm(forms.Form):
    TEMPLATE_QUESTION_NAME = 'question_%s'

    def __init__(self, questions, *args, **kwargs):
        super(TestForm, self).__init__(*args, **kwargs)
        for question in questions:
            field_name = self.TEMPLATE_QUESTION_NAME % question.pk
            self.fields[field_name] = forms.CharField(label=question.text)
