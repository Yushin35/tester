from django.views.generic import ListView, DetailView
from django.shortcuts import redirect, render
from django.urls import reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils.timezone import now


from main.models import Test, TestRun, TestRunAnswer
from main.forms import TestForm
from main.common.utils import calculate_score

# Create your views here.


class TestListView(LoginRequiredMixin, ListView):
    model = Test
    context_object_name = 'tests'
    template_name = 'tests.html'


class TestDescriptionView(LoginRequiredMixin, DetailView):
    model = Test
    context_object_name = 'test'
    template_name = 'test_description.html'

    def post(self, request, *args, **kwargs):
        test_run = TestRun.objects.create(
            user=request.user,
            test=self.get_object(),
        )
        return redirect(reverse('main:test_run', kwargs={'pk': test_run.pk}))


class TestRunView(LoginRequiredMixin, DetailView):
    model = TestRun
    context_object_name = 'test_run'
    template_name = 'test_run.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        questions = self.object.test.questions.all()
        context['form'] = TestForm(questions)
        return context

    def post(self, request, *args, **kwargs):
        test_run = self.get_object()
        questions = test_run.test.questions.all()
        form = TestForm(questions, request.POST)
        if form.is_valid():
            score = calculate_score(form.cleaned_data, questions, test_run)
            test_run.score = score
            test_run.finish_date = now()
            test_run.save(update_fields=['score', 'finish_date'])
            return redirect(reverse('main:test_score', kwargs={'pk': test_run.pk}))
        return render(request, 'test_run.html', context={
            'test_run': test_run,
            'form': form,
        })


class TestScoreView(LoginRequiredMixin, DetailView):
    model = TestRun
    template_name = 'test_score.html'
    context_object_name = 'test_run'
