from django.contrib import admin

from main.models import Test, TestRun, TestRunAnswer, Question, Answer

# Register your models here.


@admin.register(Test)
class TestAdmin(admin.ModelAdmin):
    view_on_site = True


@admin.register(TestRun)
class TestRunAdmin(admin.ModelAdmin):
    view_on_site = True


@admin.register(TestRunAnswer)
class TestRunAnswerAdmin(admin.ModelAdmin):
    view_on_site = True


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    view_on_site = True


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    view_on_site = True

