from main.models import TestRunAnswer
from main.forms import TestForm


def calculate_score(form_data, questions, test_run):
    right_answer_counter = 0
    for question in questions:
        form_question_name = TestForm.TEMPLATE_QUESTION_NAME % question.pk
        user_answer = form_data.get(form_question_name)
        right_answer = question.answers.all().filter(right=True).first().value
        TestRunAnswer.objects.create(
            test_run=test_run,
            question=question,
            text=user_answer,
        )
        if user_answer == right_answer:
            right_answer_counter += 1
    score = (right_answer_counter / questions.count()) * 100

    return score
